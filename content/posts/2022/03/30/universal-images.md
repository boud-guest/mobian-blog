+++
title = "Moving to Tow-Boot: Towards Device-Independant Images"
date = 2022-03-30T00:00:00+00:00
tags = ["mobian", "pinephone", "tow-boot"]
+++

Mobian (and most of the Arm Linux ecosystem) have been dealing with device-specific boot
sequences since our inception. However, recent changes have made it possible
to standardise these. This blog post will discuss how these changes affect Mobian
and the long term goal of creating a universal image for Mobian devices.

<!--more-->

# Every problem has a solution

Each ARM-based SoC implements its own boot sequence, looking for a bootloader at different
locations on different storage devices. For example, the OG PinePhone's Allwinner A64 looks for it
at an 8KiB offset, first on the SD card, then on the eMMC. The PinePhone Pro's RK3399 expects it to be
either on a SPI flash device, or at offset 32KiB on the eMMC or the SD card. Until now, we dealt with
those differences by generating per-device images, each one embedding a copy of the bootloader (`u-boot`
in our case) at the exact offset expected by the SoC.

However, this isn't very practical, as it prevents us from generating device-independent images,
and it can have annoying side effects: in some cases, the bootloader can remain untouched on an
SD card even after reformatting.

Fortunately, there is way to work around this problem: by letting users install the bootloader
themselves, either on the SPI flash (for devices including one) or the eMMC's boot partitions (which
are left untouched when flashing new images to the device), we can stop embedding our own copy of the
bootloader.

Following the trend across the Mobile Linux community, we therefore settled for [Tow-Boot](https://tow-boot.org/)
as the recommended bootloader and will now require PinePhone (OG and Pro versions) users to install
it before flashing a Mobian image to their devices.

# Existing Installs

The most important part of this change is that it **will not** break existing
installs. Distribution-installed u-boot is however deprecated and will not receive updates.
If you do nothing but update, you will see the `u-boot-sunxi` (PinePhone)  or
`u-boot-rockchip` (PinePhone Pro) package removed. Removing it will not stop your 
device from booting as the boot image will remain: only the bootloader copy located
on the rootfs will be deleted.

However, this package will no-longer be updated on devices supported by Tow-Boot,
although you may still receive updates from Debian's version of the package, which
won't affect the existing boot sector (neither breaking it nor updating it).
In order to install new images or receive boot firmware updates, you should
install the latest version of Tow-Boot.

# New Installs

New installs of supported devices (PinePhone and PinePhone Pro at this time) will
not come with u-boot installed. Rather, Mobian will assume that the device already
contains all firmware required to boot. If this is not the case, no need to worry,
simply go to the [Tow-Boot website](https://tow-boot.org/) and follow the 
instructions for your device.

# Universal Images

A small part of the benefit of this change is that Mobian no-longer deals with
installing and upgrading u-boot on devices. This can be tricky and potentially result
in non-booting systems.

The biggest benefit however, is the opportunity to provide a single, universal image
for mainline Linux devices. We are not there yet, but the eventual goal is to
provide a single, "as close to mainline as possible" kernel and rootfs that will work
on multiple devices.

# Frequently Asked Questions

Q: So what exactly is this Tow-Boot?
A: Tow-Boot is a user-friendly, opinionated distribution of U-Boot, where there is as few 
differences in features possible between boards, and a "familiar" user interface for an 
early boot process tool.  The goal of Tow-Boot is to make booting boring. Tow-Boot replaces
all of the functionality of Mobian's current u-boot package.

Q: Can I safely remove `u-boot-*`?
A: Removing these packages is safe, even if you have not yet installed Tow-Boot, with
one exception: `u-boot-menu` is still mandatory (and a hard dependency) on devices
using Tow-Boot.

Q: Will Mobian be packaging Tow-Boot?
A: No. This move means we do not have to manage the boot firmware and packaging it would
take us right back there. Mobian will eventually support updating Tow-Boot via fwupd when
this is supported upstream. In the mean time, the Tow-Boot installer is recommended.

Q: How do I know if I have Tow-Boot installed?
A: If your device immediately boots to a red LED and a short vibration, followed by another
vibration and the LED turning yellow, you probably have Tow-Boot.
However, installing Tow-Boot is a manual process. You would probably remember having installed
it.

Q: Should I keep `u-boot-*` around as a fallback?
A: This shouldn't be required and Mobian will not be updating u-boot on these devices going forward.
In the off-chance a Tow-Boot update goes wrong, we recommend fixing it using the Tow-Boot
installer.

Q: I've installed Tow-Boot on my original PinePhone but it's not working as described.
A: The original PinePhone will always boot any u-boot on the SD card first. If you have
an older Mobian image or another OS image on the SD card this may happen. Removing the SD
card or wiping u-boot off it should cause Tow-Boot to work.

Q: How does Mobian boot using Tow-Boot?
A: Mobian installs a u-boot configuration file at `/boot/extlinux/extlinux.conf` containing the
required boot settings. Tow-Boot can parse this and correctly boot the device directly.
Other options included UEFI booting, but using the existing extlinux setup was the least
disruptive option

# Conclusion

Some major changes to Mobian's boot sequence are being released to align with the 
direction of the community. Those changes make room for further improvements in Mobian
with the added benefit of unifying both the boot process across different devices, and
the way mobile Linux systems can be installed. We also hope hardware vendors will, over
time, follow this path and ship their devices with Tow-Boot (or a similar bootloader)
preinstalled in a location separate from the main storage.

Both new and existing users should install Tow-Boot or a similar system to provide updates
to their boot firmware. This will be required on new installs using images generated after
2022-04-02.
