+++
title = "This month in Mobian: May 2022"
date = 2022-05-31T23:59:59+00:00
tags = ["mobian", "pinephone"]
+++

This is the first in a (hopefully) series of short blog posts
summarizing what has happened in Mobianland. Given that Mobian
integrates much of the ecosystem not developed by us, this report
could reiterate many of the things being said somewhere else.
<!--more-->

# Monthly dev meeting

Despite being located all over the world (really!), we manage to meet
in person once a month. Well, ok, "meet in person" means we conduct a
jitsi video call. It is fun seeing all these sleepy faces where half
of the people would like to go to bed already, while others wished they
could have remained in bed longer.

The TL;DR:

- we will attempt to move over more repositories from gitlab to
  salsa.debian.org (given gitlabs changes to the CI allowance we get,
  and in an attempt to align ourselves closer to Debian).
- In a similar move, the developer's wiki (NOT the [user
  wiki](https://wiki.mobian.org)) moves over to the [Debian
  wiki](https://wiki.debian.org/Teams/Mobian).
- pondering if systemd-repart and systemd-growfs can maybe replace "growroot"
- We really should be opening a user-contrib section or something
  where contributed ports and kernels can live, e.g. for the Nexus
  5. Planning stage.


# OnePlus 6 / Pocophone F1

Audio was broken due to a missing config option on 5.17 kernels, and was
fixed by a subsequent update. Thanks to everyone who reported this issue
and provided the information allowing us to fix it.

# Universal Images

We have a universal mobian image that boots on the original Pinephone
and the Pinephone Pro, however, there are still some bugs to iron out
and kernel configs to be adapted. But overall, this looks promising.

However, this will require some tool that can adapt configuration
settings at runtime, based on the device it is currently running on.

# Tow-boot

We were one of the earliest distributions to move over to tow-boot,
and that worked out nearly well. However, there is a little catch:
There is no tow-boot for the PineTab yet. So all images post-March 27,
will not be able to install a U-boot if you have non installed
yet. Help to port tow-boot to the PineTab is very welcome, it should
not be too hard, but somebody needs to do it.

# PinePhone Keyboard

Mobian is now using the new PinePhone Keyboard kernel driver. As a
result, users may notice the top-row symbol keys stop working. The 
permanent fix for this can be found on the 
[mobian wiki](https://wiki.mobian.org/ppaccessories).

# Chatty and libjabber.so

You may have noticed some breakage with chatty. 
This was caused by chatty having a runpath set to be able to find 
things like libjabber.so.
These plugins used to live in `/usr/lib/purple-2` and when a recent
update changed to multiarch directory (f.e. `/usr/lib/x86-64-linux-gnu/purple-2`)
it caused 
[issues](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1011166)
[with](https://mastodon.online/@devrtz/108344777725443773)
[chatty](https://gitlab.com/mobian1/issues/-/issues/437).
We hope this was not too much of an inconvenience!

# Debian Reunion Hamburg 2022

The end of the month saw the
[Debian Reunion Hamburg 2022](https://mastodon.online/web/@debian@framapiaf.org/108351272394365597)
with an almost critically high density of linux first devices :)
This time we didn't have a talk specifically about Mobian/Debian on mobile devices,
but you might still find something interesting to
[watch online](https://mastodon.online/web/@debian@framapiaf.org/108387133781861240).
It was very fun meeting fellow Debianites in person and we look forward to 
[Debconf 2022](https://debconf22.debconf.org/).


# Upgrades
<!-- Do we just include "highlights" here or every single package that was updated/newly packaged? -->
- 2022-04-25 squeekboard 1.17.1-1 MIGRATED
  (I know strictly speaking, this is April. Hair-splitters!)
- 2022-05-04 linux-image-5.15-sunxi64_5.15.38
- 2022-05-15 Gnome Control Center 42.1
