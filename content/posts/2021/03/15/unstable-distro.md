+++
title = "The staging Mobian distro"
date = 2021-03-15T00:00:00+00:00
tags = ["Mobian"]
+++

Most of us just want their phone to be working. But **some** like to be
more on the edge of things, trying out the newest ~~bugs~~features. The
recently announced staging distribution allows users to do just
that. Read further to learn its purpose and how to enable
it.<!--more-->

Mobian is an overlay over the regular Debian *bullseye*
repository. Its package source is configured in
`/etc/apt/sources.list` with a single line: `deb
http://deb.debian.org/debian bullseye main`. [^http]

Currently the (relatively) stable Mobian overlay is configured in
`/etc/apt/sources.list.d/mobian.list` containing the following:
```
deb http://repo.mobian.org/ bullseye main non-free
```

If you had flashed your device with an image older than April 26th, 2021, you
may have a file named `/etc/apt/sources.list.d/extrepo_mobian.sources` instead,
containing the following:
```
Types: deb
Architectures: amd64 arm64 armhf
Uris: http://repo.mobian.org/
Suites: mobian
Components: main non-free
Signed-By: /var/lib/extrepo/keys/mobian.asc
```

Recently we introduced **Mobian staging**, in which we include
packages that are pretty new and/or might need a bit more
testing. Mobian staging is a full Mobian repository, so it includes
everything that the "normal" repository includes and a bit more (and
some packages a bit newer).

By switching to it, you agree to name your firstborn *Debian*
and to never eat cookies again after 3pm! So think twice before you do
it!

The easiest way you can switch to using this repository is by simply
changing the word `bullseye` to `staging` in `/etc/apt/sources.list.d/mobian.list`,
or the line `Suites: mobian` to `Suites: staging` in
`/etc/apt/sources.list.d/extrepo_mobian.sources`. This way,
you will be getting all your Mobian packages from
`https://repo.mobian.org/dists/staging` instead of
`https://repo.mobian.org/dists/mobian`.

Some of the packages you install will be from the staging Mobian repo
and most will be coming directly and unmodified from one of the Debian
mirrors.

### How do I find out if a package comes from Debian or Mobian?

Great question! Issue a `apt policy PACKAGENAME` to find possible
sources and priorities of packages. (this will be even more useful
when we talk about pinning of packages in a future blog post). For
instance, at this time `apt policy phosh` yields:
```
phosh:
Installed: 0.9.0-1mobian1
Candidate: 0.9.0-1mobian1
Version table:
 0.9.0-1mobian1 700
     700 http://repo.mobian.org staging/main arm64 Packages
     100 /var/lib/dpkg/status
 0.8.1-1mobian1 700
     700 http://repo.mobian.org mobian/main arm64 Packages
 0.8.0-1 500
     500 http://deb.debian.org/debian bullseye/main arm64 Packages
```

So, as an explanation: In this case phosh is available through Debian,
mobian or (mobian) staging. And given the priorities (those 700 and
500 numbers), it prefers the mobian ones over the Debian one. And it
prefers higher version numbers given the same priority, so it will
pick the one from the staging repo.

If you want to learn more about priorities and how apt decides what to
install, check [the debian wiki
page](https://wiki.debian.org/AptConfiguration#apt_preferences_.28APT_pinning.29)
on that topic.

### So how is Mobian staging different from Debian unstable?

For now, Mobian staging is **still** an overlay over Debian bullseye,
although this might change in a not-so-distant future. So do
not start including the Debian unstable (aka *sid*) repository,
thinking this is what you should be doing. If you include sid
repositories, you have finally voided your warranty, as far as support
from the Mobian side goes...

### Will changes in Mobian staging transition to the stable Mobian

If those changes turn out to be bad no, if yes, they will. Currently
there is no automatic transitioning of packages entering staging to
the stable version. It will need to be done by hand. The plan is to
turn on automatic transitioning at some point in the future though.

[^http]: Don't worry about the seemingly insecure **http** in
    there. All Debian/Mobian packages are signed and that signature is
    checked upon installation. As any meddling would be detected by
    that, the transport of the packages can be insecure. Using http
    allows better caching of packages "on the way" than using https.
