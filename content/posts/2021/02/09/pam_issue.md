+++
title = "Issues and hurdles when performing the initial upgrade"
date = 2021-02-09T13:53:19+01:00
tags = ["Mobian"]
+++

Dear Mobian CE owners, congratulations on receiving your mobile
phone. We hope you will like Mobian and we hope that you will be helping
us to make it even better.

We are writing this post as the initial upgrade is rather tricky and
there are a few pitfalls when performing it. We do know that real men and
women don't read manuals, but please read this post carefully in order to
not lock yourself permanently out of your phone (just an example of what
can happen if you don't read this...).<!--more-->

Most importantly, we want to inform you, that there is a minor issue
with PAM (pluggable authentication module) when upgrading the preflashed
image. It was upgraded in an incompatible manner, and when the
lockscreen of your phone is invoked after upgrading but before
rebooting, it will not accept your PIN anymore (fortunately in __most__
cases, a simple reboot makes it work again).

So do not worry, your installation is not broken!

**Therefore: After upgrading your pinephone for the first time it is
highly recommended to reboot the device after the upgrade has
finished. And keeping the screen unlocked during the initial upgrade
by:**
1. **touching the screen frequently (get used to the feeling :-)) or**
1. **temporarily disabling the automatic lockscreen in the settings.**

**Otherwise you run the risk of not being able to unlock your device
once the screen has been locked.**

The second major issue during the initial upgrade is related to the graphical
software updater, called "Software".

The *Software* application can usually keep the software on your
pinephone up-to-date and will prompt you to reboot after an update. On the
initial update with currently more than 600 package changes compared to
the factory image, it chokes and struggles, though.

You will get some mysterious "Cannot update due to unmet dependencies"
error in the software app if you try. So for the initial update, we
recommend performing a manual update in the terminal (a great way to
familiarize yourself with the terminal and Debian's package manager called apt).

For this drop into the terminal _King's Cross_ (also called **kgx**, it
has a subway logo). On the terminal issue:

```
sudo apt update
sudo apt dist-upgrade

sudo apt autoremove
```

sudo will require you to enter your PIN to allow you to perform
administrative tasks.

`apt dist-upgrade` will be required instead of the common `apt
upgrade` as it needs to remove a few packages that have been
superseded or obsoleted, and a regular "upgrade" will refuse to do
that.

(The autoremove command is not necessary, strictly speaking, but it
cleans out unneeded packages after the upgrade, you can easily run it
after a reboot too, at any time).

Again, due to the aforementioned PAM issue (it basically prevents the lock
screen from accepting your password), try to keep your screen awake
while the dist-upgrade is running in order to prevent locking yourself
out (this should only be needed on this very first upgrade). And reboot
immediately after the upgrade finished.

We are sorry for these inconveniences which are due to the fast pace of
development while your phone was being manufactured and shipped.

## Other warts and hurdles to consider

### Wifi with multiple APs

Also note that the factory image with kernel 5.9 still has a problem
when there are multiples WIFI access points providing the same network
(SSID). This has improved in the current kernel version 5.10, but you
might experience instabilities in this situation until you have fully
updated.

### Copious amounts of data downloaded

During the initial update, your pinephone wants to download 536 MB of
data (as of this writing), so make sure you are not on metered mobile
data connection or paying by the megabyte.

### Update tip: Power to the phone

Plug into power while updating to avoid your phone from going to sleep
while it updates (by default it is set to go into deep sleep only) when
running off the battery.

### Update tip: ssh for those who know what they do

If typing on the phone terminal sounds like pain, and you are familiar
with ssh, you can install the ssh server by issueing `sudo apt install
openssh-server` and then connecting to "ssh mobian@YOURPINEPHONEIP" to
access your phone from any other computer. (connect over Wifi or USB
cable to a computer) Access via ssh has the additional benefit that
the phone lockscreen will not bother you, as the ssh access remains
active even when the lock screen is invoked.

## Troubleshooting
![DONT PANIC](/images/dontpanic.jpg)

In case of fire, emergency and bricked phones:

You can always download a new image and reinstall the thing if you
really really locked yourself out. Pinephones are really hard to
permanently brick. The [installation
instructions](https://wiki.mobian.org/doku.php?id=install), and
the matrix channel [#mobian:matrix.org](https://matrix.to/#/#mobian:matrix.org) are your friends.
