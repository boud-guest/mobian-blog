+++
title = "Squashing a Book Full of Worms"
date = 2021-12-24T00:00:00+00:00
tags = ["mobian", "debian", "bullseye", "bookworm", "stable"]
+++

Now that the dust has settled on Debian's new testing repository we are focusing
our efforts on Bookworm. This long overdue blog post is a summary of what to expect in
the coming updates to Mobian.

<!--more-->

## New Software

As we outlined in [To Bullseye and Beyond]({{< ref "bullseye-release.md" >}}),
Bookworm is where Mobian and Debian will be focusing their efforts for new software.
To date, this has resulted in the introduction of GTK4 and, most importantly for PinePhone
users, Megapixels 1.3 which brings a fully hardware-accelerated camera viewfinder.

Additionally, Debian moved to GNOME 41 over the past few months, which provides newly adaptive
versions of core software such as GNOME Software and GNOME Control Center. The latter reduces
significantly the number of overflowing panels and starts significantly faster.

All of these goodies either come directly from upstream Debian or rely on newer packages
not available in Bullseye. The Mobian team will not be backporting these, so we recommend
upgrading to Bookworm to get access to them.

## How do I get access to these new packages

The good news is, if you have already switched to Bookworm, these updates are just an
`apt upgrade` away.

If you've been waiting for the dust to settle on the Bullseye release, now would be a good
time to move forward. While the turnover of new packages in Debian Testing is always high,
the major post-Bullseye transitions (such as GNOME and libc) have completed, mostly
smoothly.

If you would like to upgrade, simply replace `bullseye` with `bookworm` in
both `/etc/apt/sources.list` and `/etc/apt/sources.list.d/mobian.list`. Then, the usual
`apt update` and `apt dist-upgrade` combo will pull in all the latest packages and
upgrade your device to Bookworm. We highly recommend you **backup your home directory prior
to performing the upgrade.**

In case you originally flashed your device with an image older than April 26th, 2021,
you will have to edit `/etc/apt/sources.list.d/extrepo_mobian.sources` instead of
`/etc/apt/sources.list.d/mobian.list`, replacing `Suites: mobian` with
`Suites: bookworm`.

**Finally, do not blindly proceed with the upgrade, but pay attention to the lists of
upgraded/removed packages: in case you notice anything suspicious (such as `mobian-*`
packages being removed, for example), please abort the upgrade process and try to find out
what is going wrong.** As a last resort measure, keep in mind you can always flash a clean,
fresh image if things end up going horribly wrong.

### Fresh Installs

Since August 15th, our nightly (and now weekly) image builds are all based on Bookworm, so
if you installed from an image generated after this date, then you're all set and don't
need to do anything. Images for your device can be downloaded from
[the usual mobian images page](https://images.mobian.org).

While it is not necessary to reinstall Mobian to access Bookworm, if you are going to do so,
we recommend using one of those weekly image builds.

## A note on Repository Names

There has been some confusion about the naming of Mobian and Debian repositories
and their targets. To dispel this, we have compiled the following:

Mobian Repositories:
* `bookworm`: This is the current main repository, targeting Debian `bookworm`. Similarly to its
              Debian counterpart, it should be considered the "testing" distribution.
* `staging`: This is the bleeding edge repository for Mobian. While it also targets
             Debian `bookworm`, it contains new, minimally tested or experimental packages.
             This repository allows users to test packages before they're released to a wider
             audience and help us discover new bugs and regressions.
* `bullseye`: This is the old Mobian repository targetting Debian `bullseye`. It is no longer
              receiving updates and should now be considered a security risk, especially in the
              light of recent kernel-related CVEs.
* `mobian`: An alias for `bullseye` and therefore no longer receiving updates as well.

Debian Repositories:
* `bullseye`: Current Debian stable.
* `bookworm`: Current Debian testing and the target of all supported Mobian repositories.
* `sid`: Debian unstable. While conceptually similar, this is not targeted by Mobian `staging`.

Simply put, Mobian `bookworm` and Mobian `staging` are the current, supported versions, both
targeting Debian `bookworm`.

As you may have noticed, there is no Mobian "stable" repository at the moment: while a
significant (and ever growing) number of users deem Mobian stable enough for daily use,
there are still a number of issues getting in the way of a really smooth and enjoyable
user experience. It is our hope and goal that those issues will be fixed during the Bookworm
development cycle, leading to a first Mobian stable release by the time Bookworm becomes
the new Debian stable (fingers crossed).

## A note on Kernel Releases

The current plan is to have the most recent LTS kernel available in Mobian `bookworm`, with
`staging` receiving less well-tested non-LTS versions. This is due to the regressions
which can occur between kernels and allows users to understand before upgrading how
thoroughly a given kernel has been tested.

In saying this, 5.15 is the latest LTS kernel and is currently the default one in Mobian
`bookworm` as well as Mobian `staging`, with the latter about to switch to 5.16 once it is
released.

Mobian users who want to help with this process are encouraged to test these unstable kernels
when they are released and report any regression.
