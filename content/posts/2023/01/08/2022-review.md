+++
title = "A look back in the mirror... And a glimpse of the future!"
date = 2023-01-08T00:00:00+00:00
tags = ["mobian", "2022"]
+++

2022 has been an extremely busy year for Mobian developers, and a lot has
happened throughout this period. As we're entering a new ~~development
cycle~~ year, now is a good time to look back at what we achieved in 2022.

<!--more-->

## Foundation work

One of our goals has always been for Mobian to slowly dissolve into Debian. As
such, we aim at moving packages from the downstream Mobian archive into the
upstream Debian repositories.

### Summer migration

As a consequence, we decided to move all the source code used in Mobian from
[gitlab.com](https://gitlab.com) to [Salsa](https://salsa.debian.org), Debian's
own [GitLab](https://about.gitlab.com/) instance, during last summer. With the
exception of non-free firmware blobs of unclear origins, Mobian is now fully
maintained within Salsa's [Mobian-team](https://salsa.debian.org/Mobian-team)
group. It should be noted that, thanks to GitLab's export/import features, this
important migration has been mostly painless!

### Feeding the Debian package servers

We also welcomed the contributions of Debian developers who helped us both
for improving Mobian and for moving our downstream packages to Debian upstream.
Together, we were able to push a great number of new packages to the Debian
archive, including (but not limited to):
  * [calamares-extensions](https://tracker.debian.org/pkg/calamares-extensions)
  * [eg25-manager](https://tracker.debian.org/pkg/eg25-manager)
  * [mobile-config-firefox](https://tracker.debian.org/pkg/firefox-esr-mobile-config)
  * [numberstation](https://tracker.debian.org/pkg/numberstation)
  * [passes](https://tracker.debian.org/pkg/passes-gtk)
  * [phosh-antispam](https://tracker.debian.org/pkg/phosh-antispam)
  * [powersupply](https://tracker.debian.org/pkg/powersupply-gtk)
  * [satellite](https://tracker.debian.org/pkg/satellite-gtk)
  * [sxmo-utils](https://tracker.debian.org/pkg/sxmo-utils)

This past year also saw us upload
[calamares-settings-mobian](https://tracker.debian.org/pkg/calamares-settings-mobian)
and [plymouth-theme-mobian](https://tracker.debian.org/pkg/plymouth-theme-mobian)
to Debian, making those the first Mobian-specific packages to make it into the
Debian archive!

### Less downstream tweaks

Over the past year, one of our main areas of work was getting rid of as much
custom scripts and tweaks as possible, and rely on existing upstream solutions
instead.

A good example of such improvements if the switch to `systemd` services for
growing the root filesystem on initial boot: while previously relying on
[cloud-initramfs-tools](https://tracker.debian.org/pkg/cloud-initramfs-tools)
and a downstream initramfs script, we now hand this task over to
[systemd-repart](https://www.freedesktop.org/software/systemd/man/systemd-repart.html)
and [systemd-growfs](https://www.freedesktop.org/software/systemd/man/systemd-growfs.html).

We also got rid of device-specific kernel cmdline parameters, as we can now
rely on the device tree for providing this information, such as the default
serial console; similarly, by working with upstream to automatically compute
display scaling in [phoc](https://gitlab.gnome.org/World/Phosh/phoc), we could
drop our downstream, per-device `phoc.ini` files.

Overall, those changes allowed us to get rid of most of our per-device tweaks,
up to the point where now we barely need any. This not only makes it easier to
envision out-of-the-box Debian support for the devices we currently work on,
but will also make new device bring-up incredibly easier!

## Being good FLOSS citizens

Over the past year, Mobian developers have also tried to fix issues and add
features upstream rather than carrying patched packages. Notable upstream
projects we contributed to in 2022 include, among others:
* [calamares](https://calamares.io/)
* [linux](https://salsa.debian.org/Mobian-team/devices/kernels/rockchip-linux/-/merge_requests/24)
* [mobile-config-firefox](https://gitlab.com/postmarketOS/mobile-config-firefox)
* [phoc](https://gitlab.gnome.org/World/Phosh/phoc)
* [phosh](https://gitlab.gnome.org/World/Phosh/phosh)
* [sxmo](https://sxmo.org/)
* [systemd](https://systemd.io/)
* [u-boot-menu](https://salsa.debian.org/debian/u-boot-menu)

We also keep maintaining software that are an important part of the mobile
Linux ecosystem, such as [eg25-manager](https://gitlab.com/mobian1/eg25-manager)
and [callaudiod](https://gitlab.com/mobian1/callaudiod). Lately, we published
2 additional projects, not only because we felt those were needed for Mobian,
but also because they might be useful to other distros and users.

Those projects are [droid-juicer](https://gitlab.com/mobian1/droid-juicer)
(more on this one below) and [phog](https://gitlab.com/mobian1/phog), which is
a GUI login/session startup application aimed specifically at mobile devices
(you can think of it as "GDM for phones"). It relies on
[greetd](https://git.sr.ht/~kennylevinsen/greetd) for performing the actual
session startup, so `phog` doesn't interact with `pam` nor `systemd` directly.

Bringing a graphical login manager to mobile devices allows our users to have
multiple user accounts instead of the single (and so-far hardcoded) `mobian`
user. They can also use `phog` to select which graphical environment they want
to start, so they could very well setup one user for work running Phosh, and
another user for their everyday life running SXMO, for example. Finally, `phog`
ensures the GNOME keyring is unlocked when the session is started so we can
get rid of the annoying "keyring password" dialogs on session startup :)

## Not quite there yet

Unfortunately, time is not extensible: as volunteers we can't spend as much
time working on Mobian as we'd like, meaning we sometimes have to prioritize
things, and delay some of the work we had hoped to achieve.

This is exactly what happened with the [universal images]({{< ref "universal-images.md" >}})
that we hoped to implement in 2022, so they could be ready, tested and debugged
come 2023, and finally be made our new default during the 1st quarter of this
year.

To this effect, we started working on fundational pieces of software, starting
with [tweakster](https://crates.io/crates/tweakster), a utility to manage
device-specific config at runtime. It, in turn, gave birth to
[droid-juicer](https://tracker.debian.org/pkg/rust-droid-juicer), a tool
designed to extract proprietary firmware from vendor partitions on Android
devices: that way, we wouldn't have to package non-free firmware for those
devices anymore, which increases our workload while decreasing the chances of
getting such devices properly supported by vanilla Debian.

The good point is that the initial release of `droid-juicer` has been uploaded
to Debian before the year ended; the less-good point is that `tweakster` and
consequently universal images have been delayed by this work and therefore will
have to wait for the [Trixie](https://wiki.debian.org/DebianTrixie) development
cycle. However, we might have a few more tricks up our sleeves to make at least
*some* of it happen earlier ;)

## Community vitality

Finally, 2022 showed once again the mobile Linux community vitality through
numerous events and collaborations. In particular, FOSDEM 2022 was the first
edition including a "FOSS on Mobile Devices" devroom, which ended up being
incredibly successful.

It went so well, actually, that this year's (in-person) FOSDEM will feature
*both* the [devroom](https://fosdem.org/2023/schedule/track/foss_on_mobile_devices/)
and a "Linux on Mobile" [stand](https://fosdem.org/2023/stands/) where you'll
be able to discuss with members of several projects, including Mobian,
[postmarketOS](https://postmarketos.org/), [UBPorts](https://ubports.com/),
[Sailfish OS](https://sailfishos.org/) and more!

We also agreed with other mobile distros to switch to Tow-Boot for supported
devices, eventually leading PINE64 to ship the PinePhone Pro with this
booloader factory-flashed. We expect such cross-distro collaboration to keep
covering more areas over time and we trust this, along with the great
relationship we have with developers from [postmarketOS](https://postmarketos.org/)
and [Purism](https://puri.sm), will lead to even more spectacular improvements
to the ecosystem we're shaping up together.

We're also seeing two important trends both in the market and community,
starting with new manufacturers joining the dance:
* Juno Computers released an Intel-based
  [Linux tablet](https://junocomputers.com/product/juno-tablet/) shipping with
  either Mobian or Manjaro as the default OS
* FydeOS announced its [FydeTab Duo](https://fydetabduo.com/), a 12" tablet
  based on the Rockchip RK3588 flagship SoC; although it comes with FydeOS, a
  ChromiumOS derivative, the [tech specs](https://fydetabduo.com/specs) page
  mentions optional compatibility "with other open-source operating systems,
  including popular Linux distributions"

Another trend is the fact the PinePhone Pro is being adopted widely as a
development platform for showcasing various projects, as can be seen from
CodeThink's [recent work](https://www.codethink.co.uk/articles/2022/gnome-os-mobile/)
on a mobile version of GNOME OS.

Both those trends attract more users and developers to our platform(s),
improving the likelihood that *someone* (who might just be you!) will work on
solving your particular problem in a way that will benefit the whole community.

## Our plans for 2023

With the Bookworm [freeze process](https://release.debian.org/bookworm/freeze_policy.html)
starting in just a few days, this year will be very special regarding the way
we work on Mobian: uploads to Debian will likely slow down starting Feb 12th
(soft freeze) and come to a near-halt 1 month later (hard freeze). This implies
we will then be only able to focus on Mobian-specific packages until Bookworm
is finally released.

### Keep moving while frozen

Fortunately (or not?), there are always a lot of non-packaging tasks pending,
so we have at least *some* idea how we're going to spend our time: not working
on Mobian/Debian packages means we have then more time to hunt down bugs and
improve the overall system stability.

This will also be a good opportunity to finally get to the upstream
contributions we've been holding back for quite some time. This includes,
for example, getting [Tow-boot](https://tow-boot.org/) to support the
[PineTab](https://www.pine64.org/pinetab/) and [Librem 5](https://puri.sm/products/librem-5/)
(no promise though), or pushing more of our kernel patches upstream.

We'll also have more time to work on new hardware support, the
[PineTab 2](https://www.pine64.org/2022/12/15/december-update-merry-christmas-and-happy-new-pinetab/)
and [FairPhone 4](https://shop.fairphone.com/en/buy-fairphone-4) being good
candidates for official Mobian support.

### Mobian Stable

With the release of Bookworm in 2023 the first Mobian stable release will
finally happen! This version will follow the same release cycle as Debian,
providing only bugfix and security updates. Most of those will come directly
through Debian, including for Mobian-specific packages we still hope to get
accepted before the soft freeze begins, miniramfs being the most important of
those.

We'll still need to carry downstream packages though, and will do our best to
provide timely security updates and bugfixes for those. To this effect, we
settled to ship Linux 6.1 on all our devices as it should be the next LTS
kernel, and are committing to keeping it as up-to-date as possible.

### The day(s) after

Once Bookworm is released, we'll switch our testing and staging repos to track
Debian Trixie and will keep bringing you up-to-date software during the whole
Trixie development cycle.

Now that we have [SXMO](https://sxmo.org/) available in Debian, and good
progress is being made towards packaging [Plasma Mobile](https://plasma-mobile.org/),
we also hope the Trixie development cycle will give us the opportunity to provide
our users with more graphical environment options. We'll also closely track the
work of the [Debian UBPorts team](https://qa.debian.org/developer.php?email=team%2bubports%40tracker.debian.org)
with the hope that we can ultimately provide [Lomiri](https://lomiri.com/)-based
images at some point.

Finally, we'll keep working towards making universal images happen, while still
looking at and packaging mobile-friendly software, such as
[Organic Maps](https://organicmaps.app/) among others.

## What we'd love to see happening in 2023

As mentioned earlier, a lot is constantly happening throughout the whole
ecosystem, which in itself is very positive. However, we've noticed some
encouraging activity in recent months which fills us with hope for this new
year.

After the [initial submission](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=78a21c7d59520e72ebea667fe8745a4371d9fe86),
the PinePhone Pro is getting [better](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=a7e6dbd6ac034480d7fde95ecf2259d27e5655b1)
and [better](https://patchwork.kernel.org/project/linux-arm-kernel/list/?series=706595)
upstream support as time goes by. As several different developers have
contributed over these past few months, we hope this trend will carry on, and
maybe even join forces so we can finally have a device fully supported
by the mainline kernel.

The other area triggering our interest is all the work currently happening in
[libcamera](https://libcamera.org): developers from this project envision
mobile Linux devices as a perfect showcase for `libcamera`, and therefore do
their best in order to ease contributions, fix camera sensor drivers for better
libcamera support, and more generally improve the camera situation on Linux. We
can't wait to see where we'll be standing a year from now, and are confident
new applications and better support for our devices isn't too far now!

## Final word

We hope you're as excited as we are about the current state and foreseeable
evolutions of the mobile Linux ecosystem, and wish you all a happy year 2023!
