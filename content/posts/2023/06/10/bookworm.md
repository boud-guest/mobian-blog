+++
title = "The Bookworm has landed!"
date = 2023-06-10T12:00:00+00:00
tags = ["mobian", "debian", "release", "bookworm"]
+++

## Debian has released "Bookworm" and so has Mobian!

According to schedule and on time Debian has managed to put out the Bookworm
release. Congratulations! In step with that release, Mobian is herewith
officially releasing its first stable release, codenamed ... tadaa ...
"Bookworm"!

<!--more-->

## What does this mean?

A lot has happened since the release of the [Mobian community edition
PinePhone]({{< ref "mobian-community-edition.md" >}}). But ever since its
inception, Mobian has been tracking the testing repository of Debian. The
previous release - Bullseye - was just not deemed suitable for mobile devices
yet.

Fortunately, a lot has happened since then, and we can proudly claim that
Debian Bookworm is the first release that we deem to be mobile-friendly enough.
It is used as the base for our stable Mobian release.

This means that, from now on, you will have the choice between sticking with
the first ever stable release or switching over to the next development
"branch".

### Which release is the right one for me?

If you stick with Bookworm you will be getting a stable system, not in the
sense of bug-free, never-crashing software (does that even exist?), but as a
fixed set of software components that are known to work well together and don't
rely on undefined behavior. The only updates you will receive will be targeted
ones, fixing either a major bug or a security vulnerability, and will be
thoroughly tested to minimize the risk of regression.

With Bookworm, you can *depend* on your device (to the extent permitted by the
stability of the included software) without having to fear that an upgrade
might alter its behavior in a negative way.

This approach has one downside, though: version numbers for Bookworm packages
will be mostly frozen (except for aforementioned targeted updates) and you will
not receive any update bringing new functionality to your applications.

New features will only be arriving in the *testing* repositories of both Debian
and Mobian, as they convey the risk of introducing major bugs, and as such
should be reserved to users with sufficient knowledge of their system's
internals and the ability to manually repair things that can (and **will**!)
break over time.

So in the end, you have to pick your poison: stick with a dependable device
with only targeted updates, or live fast on the edge of the new Debian testing
(which will become Trixie) and ~~die young~~ accept the fact that updates can
occasionally break your system pretty badly. We will not be making that
decision for you.

### How to stick to stable Mobian?

Unless you previously enabled the [staging repo]({{< ref "unstable-distro.md" >}}),
chances are you already track Mobian Bookworm and don't need to do anything.
You should ensure `/etc/apt/sources.list` contains the following lines:

```
deb http://deb.debian.org/debian bookworm main non-free-firmware
deb http://security.debian.org/ bookworm-security main non-free-firmware
```

*Notes:*
  - *if your `sources.list` is lacking `non-free-firmware` but your device
    works as expected, you don't need to add this component*
  - *if the entries contain `non-free`, please change those to
    `non-free-firmware` unless you actually want to install non-free
    packages other than firmware (in which case you probably should append
    `non-free-firmware` anyway)*

Similarly, the `/etc/apt/sources.list.d/mobian.list` file should have the
following contents:

```
deb http://repo.mobian.org/ bookworm main non-free-firmware
```

*Notes:*
  - *`non-free` **should** be replaced with `non-free-firmware` in this file as
    well; both components are identical for now, but we plan to drop `non-free`
    during the Trixie development cycle, so switching early will ensure your
    system is ready for future major upgrades*
  - *if you don't have a `mobian.list` file under `/etc/apt/sources.list.d/`
    but `extrepo_mobian.sources` instead, ensure the `Suites:` line's value
    is `bookworm` and `Components:` contain `main` and (optionally)
    `non-free-firmware`; alternatively, you can delete `extrepo_mobian.sources`
    and create `mobian.list` with the expected contents*

We did our best to keep a clean upgrade path even for old Mobian installs.
However, there's no way we can address every possible (corner) case, and we
expect some users (especially those with really old installs) will run into
issues we didn't foresee while upgrading to bookworm. Therefore, we strongly
recommend that you start from a clean system by flashing the Bookworm image
corresponding to your device (backing up and restoring all your important files
in the process, of course). This will ensure you get rid of leftover obsolete
tweaks and benefit from all the improvements we made over time.

### What about bugfix and security updates?

You will notice that the [Mobian repository](https://repo.mobian.org/dists/)
has grown a new entry: __bookworm-updates__. This is intended to be the
repository where all our stable updates land, including security fixes. The
latter will however quickly migrate to the main bookworm repo (from a few hours
to a few days, depending on the criticity of the vulnerability and potential
side-effects of the proposed fix). "Normal" (i.e. bugfix-only) updates will
migrate to `bookworm` for each new point release of Debian Bookworm.

We welcome users willing to test those updates (and report any regression they
might cause) before they migrate, as this would help us maintain the stability
of the overall distribution; those users should therefore add the following
line to their `mobian.list` file:

```
deb http://repo.mobian.org/ bookworm-updates main non-free-firmware
```

*Please be aware that doing so will expose you to issues/regressions caused by
updated packages, though the risk will be far less important than users opting
for the development repositories.*

### How to track the "new" testing (Trixie)?

If you have chosen the Red Pill, you should first ensure both your
`sources.list` and `mobian.list` files are configured appropriately (see above
section) and **MUST** be 100% sure your system is fully up-to-date (ideally
from a freshly flashed Bookworm image).

You can then execute the following steps:
  * drop the `bookworm-security` line from `sources.list` and replace
    `bookworm` with `trixie` in the remaining line
  * replace `bookworm` with `trixie` in `mobian.list` (drop the
    `bookworm-updates` line if you previously added it)

**Important:** Be aware that in the weeks following a Debian stable release
there is a huge influx of updated packages and big transitions to new versions,
clearing the backlog that has been building up during the freeze period. We
would advise people to not immediately switch over to the new Trixie/testing
distribution. Wait for a few weeks/months until things have calmed down and
switch over then, unless you want to live through every breakage that might
ensue post-release.

## Where are the downloads and installers for Mobian Bookworm?

As usual, images can be found [here](https://images.mobian.org/).
Just select your device (family) and download the image.

For the [PinePhone](https://images.mobian.org/pinephone/installer/)
and the [PinePhonePro](https://images.mobian.org/pinephonepro/installer/)
installer images are available in addition to the regular live images.

Instructions for verifying and flashing the image can be found below.

## Are there installation instructions?

Instructions verified to be working by a Mobian/Debian Developer can be found
in the Debian wiki:
 
 - [Librem 5](https://wiki.debian.org/InstallingDebianOn/Purism/Librem5Phone)
 - [PinePhone](https://wiki.debian.org/InstallingDebianOn/PINE64/PinePhone)
 - [PinePhone Pro](https://wiki.debian.org/InstallingDebianOn/PINE64/PinePhonePro)
 
 For other devices (or ones which haven't been blessed by a Mobian/Debian
 Developer) please see the [community wiki](https://wiki.mobian.org/doku.php?id=install).
 
## A peak behind the curtain
 
We (and the larger community) have been very busy in the last two years
and we're excited to ship (and maintain going forward) our first stable image!

Here are some of the highlights of this release:
  - Solid support for MMS, visual voicemail and other bits and pieces (thanks
    kop316!)
  - Kernel CI machinery which eases rebasing patches needed for our supported
    devices (thanks a-wai and undef!)
  - [droid-juicer](https://gitlab.com/mobian1/droid-juicer) for extracting
    firmware from Android devices (thanks a-wai!)
  - GNOME core application all adaptive (modulo the cases where they aren't)
    (thanks GNOME!)
  - [phosh](https://phosh.mobi) and related components have seen all sorts of
    improvements (thanks agx!)
  - More mature SIP support in Calls (thanks devrtz!)
  - sxmo/swmo made it into Debian (thanks jochensp!)
  - Plasma Mobile core packages also made it into Debian, although very few
    applications are available for now (thanks Marco!)
  - [phog](https://gitlab.com/mobian1/phog) (the PHOne Greeter) can be used to
    switch the "desktop" environment on login (thanks a-wai!)
 
Of course there have been a lot more added features, fixed bugs and other
improvements which are way too numerous to list. Have a look
[here](https://www.debian.org/News/2023/20230610) for more details on what
changed in Debian.

If you want to know more nitty gritty details how we prepared for bookworm,
check out our [Bookworm release TODO/checklist](https://wiki.debian.org/Teams/Mobian/Meetings/2023-06-03),
as well as the activity on the [Mobian](https://salsa.debian.org/Mobian-team)
and [DebianOnMobile](https://salsa.debian.org/DebianOnMobile-team/) teams on
Salsa.
