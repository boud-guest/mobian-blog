+++
title = "These months in Mobian: February/March 2023"
date = 2023-03-10T00:00:00+00:00
tags = ["mobian", "TMIM"]
+++

March isn't even over yet and we are already summing up what has
happened in both February and March. Dang, are we fast...

<!--more-->

## FOSDEM

February 2023 began with a bang: finally a physical [FOSDEM
conference](https://fosdem.org/2023/) in Brussels, and a few (or
rather, plenty of) mobile Linux people organized a physical devroom on
"FOSS on mobile Linux". Many submissions on various topics, from many
distros and projects, of which 10 were selected.

![A-wai next to his presentation in the dev
room](https://cdn.fosstodon.org/media_attachments/files/109/807/554/702/252/011/original/40bf37281b12cb54.jpg
"a-wai explaining Mobian at FOSDEM") (Photo by [spaetz](https://fosstodon.org/@mobian/109807566055878705) under CC-0)

Directly relevant were talks on "[What's new in the world of phosh?](https://fosdem.org/2023/schedule/event/phosh/)" and
"[Mobian: to stable... and beyond!](https://fosdem.org/2023/schedule/event/mobian_to_stable_and_beyond/)". But also others, such as the [talk on ondev2](https://fosdem.org/2023/schedule/event/ondev2_installer/), a new
cross-distro installer, which Mobian will potentially use. Many other
interesting talks on convergence, so if you have not already, check
out the [whole track](https://fosdem.org/2023/schedule/track/foss_on_mobile_devices/).

Another highlight was the cross-distro stand with an unusual high
density of Linux phones being put on a single table, and a dense
crowd of people being interested in Mobile on Linux. Some were
well-know names and faces, that are already familiar with the state of
things, while others were surprised that Linux on Mobile "is a thing".

![A table full of mobile Linux phones](https://cdn.fosstodon.org/media_attachments/files/109/812/594/706/152/480/original/6b01d35e58a723ae.jpeg
"A table full of mobile phones running all kinds of weird and
wonderful stuff")

Most importantly though, it was one of those rare moments in life to
physically meet your collaborators. And while we did not manage to do
a live-podcast as the postmarketOS contributors did, we did have beer,
dinner, talks, and fun together. Sometimes, these are the most
important times in a collaboration!

And yes, we will try to have Mobian stickers next time around! :-)

## Website

Our Website does not contain a lot of content, but previously it still
used some Javascript in order to provide a flying-in animation (and
possibly other things). Thanks to community member Sven J. Grewe, we
have now done away with the Javascript entirely (still keeping the
animation) and our root page now consists of 1 html, 1 css and 7
icons/images, loading pretty fast. Thank you Sven, contributions are
always welcome!

## Debian Bookworm

Debian has started its soft freeze towards the Bookworm release. This
implies: no new packages, and existing ones have a longer
transitioning time from unstable into Bookworm. In a few days, the
hard freeze will occur. This will also stop the inflow of new
versions, in order to settle on a stable basis for Bookworm. What does
this mean for Mobian? We won't benefit from new releases and versions
of upstream projects anymore (which is bad) but we will be able to
start focusing on getting what we already have into a good shape. The
Pinephones currently run on kernel 6.1, providing a decent and current
release. As announced in a-wai's talk, there will be a stable
bookworm-based mobian, and some time after the bookworm release, we
will start tracking the next testing release (Trixie) in addition to
the stable branch.

There are a few glitches that still need to be fixed before a possible
release, for instance, mesa 22.3.6 contains an important bug fix,
preventing some applications from crashing, that we hope will make the
transition into bookworm soonish.

We are aware that bluetooth has gotten a bit more unreliable in the
past few weeks, but we are not exactly sure why yet. It is *not* just
regressions because we (or rather Debian) switched from pulseaudio to
pipewire. Unfortunately debugging Bluetooth is not easy.

Overall, we are pretty happy with the current state of things and
confident that Bookworm will be the first Debian distribution that can
provide a good stable base for a mobile device.

## Plasma beams on full strength!

We are very happy that it was possible to squeeze in Plasma mobile
into the Debian repository right before the freeze. Nobody can accuse
us of being a single window manager distro. Phosh, SXMO and Plasma are
all available at your choice. Beam me up, Scotty!
