+++
title = "The non-free-firmware repository"
date = 2023-03-11T12:00:00+00:00
tags = ["mobian", "debian", "non-free", "firmware"]
+++

## PinePhone and PinePhonePro Owners (and others), please change your config

PinePhonePro users might get nagging messages about packages being held back right now...

**TL;DR:** Please add the `non-free-firmware` section to the Debian repository line in `/etc/apt/sources.list`.
<!--more-->

## Introducing the non-free-firmware section

**UPDATE**: slightly reworded first paragraph.

Piggybacking on Debian is great, but it also means we need to track changes that occur in Debian.

The Debian project has [decided](https://www.debian.org/vote/2022/vote_003#textb) to include non-free-firmware in their installer images for hardware enablement reasons
and a better out of the box user experience.
Therefore firmware file have been moved out of the `non-free` section and into the new **`non-free-firmware`** section in the [debian repository](https://ftp.debian.org/debian/dists/bookworm/).

Why does this matter to Mobian users? The Wifi firmware for the PinePhone and PinePhonePro has migrated to the upstream Debian repository and the [firmware-realtek-rtl8723cs-bt](https://packages.debian.org/bookworm/firmware-realtek-rtl8723cs-bt) (PinePhone) and [firmware-brcm80211](https://packages.debian.org/bookworm/firmware-brcm80211) (PinePhone Pro) packages are now in the new `non-free-firmware` section. This means at least PinePhonePro users need to add that section now, but it does not hurt for other devices too and we recommend everyone to make this change.

So mobian users, head over to the file `/etc/apt/sources.list` and edit the line starting with `deb http://deb.debian.org`. It should end with (some of) the sections `main non-free`. Just change `non-free` to `non-free-firmware`, or add `non-free-firmware` (delimited by a space) at the end of that line if you still want access to non-free software which are not only firmware packages.

Finally, `apt update && apt upgrade` and you should be on the latest firmware on the upstream Debian repository.

If you've lost network access on your PinePhone Pro, you can download the package manually using the above link (or `apt download firmware-brcm80211` on another Debian Bookworm machine), transfer the file to your phone over USB and install it manually with `dpkg -i firmware-brcm80211_20230210-2_all.deb`.
