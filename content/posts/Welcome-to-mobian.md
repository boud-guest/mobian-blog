+++
title = "Welcome to Mobian's Blog!"
tags = ["mobian", "blog"]
date = "2021-01-13"
+++

Almost a year after the project first started, it's about time we created an
official communication channel for more-than-140-chars messages from the Mobian
team.

<!--more-->

On this blog you will find status updates, important announcements and technical
articles by our developers.

Make sure you bookmark this website to stay updated on the latest Mobian news.

![The Mobian family](/images/mobian-family.jpg)
